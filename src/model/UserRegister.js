export class UserRegister {
    constructor(username, firstName, lastName, description, birthDate, password) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.description = description;
        this.birthDate = birthDate;
        this.password = password;
    }
}