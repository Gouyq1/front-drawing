export class UserDisplay {
    constructor(id, username, firstName, lastName, description, birthDate, password) {
        this.id = id
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.description = description;

        if(birthDate !== undefined) {
            if(birthDate.length === 10) {
                this.birthDate = birthDate;
            } else {
                // Champ date stocké "2022-01-18T00:00:00.000+0000" en BDD.
                this.birthDate = birthDate.substring(0, 10);
            }
        } else {
            this.birthDate = null
        }
        
        this.password = password;
    }
}