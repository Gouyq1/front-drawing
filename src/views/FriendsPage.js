import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'

import { ChatComponent } from '../components/chat/ChatComponent'
import { FriendsListComponent } from '../components/social/FriendsListComponent'

export const FriendsPage = () => {
    return (
        <Row>
            <Col>
                <FriendsListComponent></FriendsListComponent>
            </Col>
            <Col>
                <ChatComponent></ChatComponent>
            </Col>
        </Row>
    )
}