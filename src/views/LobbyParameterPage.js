import Row from 'react-bootstrap/Row'

import { GameConfigurationComponent } from '../components/lobby/GameConfigurationComponent'


export const LobbyParameterPage = () => {
    return (
        <GameConfigurationComponent context='lobbyParameter'></GameConfigurationComponent>
    )
}