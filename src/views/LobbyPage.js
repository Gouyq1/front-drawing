import { useDispatch, useSelector } from 'react-redux'

import { selectCurrentRoom, selectUser } from '../core/selectors';

import { useEffect } from 'react'

import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import ButtonGroup from 'react-bootstrap/ButtonGroup'
import Button from 'react-bootstrap/Button'
import Stack from 'react-bootstrap/Stack'

import { useHistory } from "react-router"

import { GameConfigurationComponent } from '../components/lobby/GameConfigurationComponent'
import { GameTitleComponent } from '../components/game/GameTitleComponent'
import { ChatComponent } from '../components/chat/ChatComponent'
import { GamePlayersListComponent } from '../components/lobby/GamePlayersListComponent'
import RoomService from '../services/RoomService';
import { setCurrentRoom } from '../core/actions';

import GameService from "../services/GameService";

export const LobbyPage = () => {

    const room = useSelector(selectCurrentRoom)
    const user = useSelector(selectUser)

    const history = useHistory()
    const dispatch = useDispatch()

    const handleLaunchGame = () => {
        GameService.getInstance().startGame(room.id);
    }

    const handleLeaveGame = () => {
        // Si l'hôte quitte, on supprime la partie.
        if(room.roomOwner === user.id) {
            RoomService.getInstance().deleteRoom(room.id).then((response) => {
                // Rien.
            })
        } else {
            RoomService.getInstance().leaveRoom(room.id, user.id).then((response) => {
                // Rien.
            })
        }

        let roomEmpty = {
            id: null,
            roomName: "",
            roomOwner: "",
            isPrivate: false,
            rounds: 5,
            maxPlayers: 5,
            timeToDraw: 30,
            code: "",
            players: []
        }
        dispatch(setCurrentRoom(roomEmpty))

        history.push('')
    }

    useEffect(() => {
        RoomService.getInstance().getRoomById(room.id).then((response) => {
            response.json().then((value) => {
                console.log(value)
                dispatch(setCurrentRoom(value))
            })
        })
    }, [])

    return (
        <Row>
            <Col xs={3}>
                <GameConfigurationComponent room={room} context='lobby'></GameConfigurationComponent>
            </Col>
            <Col>
                <GameTitleComponent room={room}></GameTitleComponent>
                <GamePlayersListComponent playersIds={room.players}></GamePlayersListComponent>
                
                <Stack gap={3}>
                    <ButtonGroup>
                        { (room.roomOwner === user.id) && (
                            <Button
                                onClick={handleLaunchGame}
                                variant='outline-primary'>
                                Lancer la partie
                            </Button>
                        )}
                        <Button
                            onClick={handleLeaveGame}
                            variant='outline-danger'>
                            Quitter la partie
                        </Button>
                    </ButtonGroup>
                </Stack>
            </Col>
            <Col xs={3}>
                <ChatComponent></ChatComponent>
            </Col>
        </Row>
    )
}