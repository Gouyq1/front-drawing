import { useState, useEffect } from 'react'

import { useDispatch, useSelector } from 'react-redux'
import { setCurrentRoom, setRooms } from '../core/actions';

import { selectCurrentRoom, selectRooms, selectUser } from '../core/selectors'

import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Button from 'react-bootstrap/Button'
import Card from 'react-bootstrap/Card'
import Form from 'react-bootstrap/Form'
import Alert from 'react-bootstrap/Alert'

import { GameListComponent } from '../components/game/GameListComponent'

import { RoomService } from '../services/RoomService'
import { useHistory } from 'react-router';
import { RoomSocket } from '../sockets/RoomSocket';

import SockJS from 'sockjs-client';
const Stomp = require('stompjs');


export const HomePage = () => {

    const history = useHistory()
    const dispatch = useDispatch()
    const room = useSelector(selectCurrentRoom)
    const user = useSelector(selectUser)
    const rooms = useSelector(selectRooms)

    const [roomCode, setRoomCode] = useState()
    const [displayWarningNotFound, setDisplayWarningNotFound] = useState(false)

    const handleInputChange = (event) => {
        setRoomCode(event.target.value)
        setDisplayWarningNotFound(false)
    }

    const handleJoinPrivateGame = () => {
        const privateRoom = allRoomsList.filter(function (room) {
            return room.code === roomCode
        })
        
        if(privateRoom.length === 0) {
            setDisplayWarningNotFound(true)
        } else {
            dispatch(setCurrentRoom(privateRoom[0]))
            RoomService.getInstance().joinRoom(room.id, user.id)
            history.push('lobby')
        }
    }

    const getWarningNotFound = () => {
        return (
            <Alert variant="warning" className="m-3">
                Aucune partie privée avec le code "{roomCode}" trouvée.
            </Alert>
        )
    }

    const handleCreateGame = () => {
        history.push("/lobbyParam")
    }

    const [allRoomsList, setAllRoomsList] = useState([])
    //const [publicRoomsList, setPublicRoomsList] = useState([])

    useEffect(() => {
        // Si on est déjà dans une partie, on est immédiatement redirigé vers le lobby.
        if(room.id !== null) {
            history.push('lobby')
        }

        RoomService.getInstance().getAllActiveRooms().then((response) => {
            response.json().then(function(rooms) {
                if(response.status === 200) {
                    setAllRoomsList(rooms)

                    // Get all rooms that are not private.
                    if(rooms.length != 0){
                        rooms = rooms.filter(function (room) {
                            return !room.privateRoom
                        })
                    }
                    
                    dispatch(setRooms([...rooms]))
                } else {
                    //alert("The user does not exists... Please check the fields!")
                }
            })
        })

        /*RoomSocket.getInstance().listenRoomCreation((roomInfo) => {
            setAllRoomsList(allRoomsList.push(roomInfo))
        })

        RoomSocket.getInstance().listenRoomListDeleted((roomId) => {
            setAllRoomsList(allRoomsList.filter(room => room.id != roomId))
        })

        RoomSocket.getInstance().listenRoomListUpdated((roomInfo) => {
            let list = allRoomsList.filter(room => room.id != roomInfo.id);
            setAllRoomsList(list.push(roomInfo))
        })*/
    }, [])

    return (
        <Row>
            <Col>
                <Button onClick={handleCreateGame}>
                    Créer une partie
                </Button>
                <hr/>
                <Card>
                    <Card.Header>
                        Rejoindre une partie
                    </Card.Header>
                    <Card.Body>
                    <Form.Label>Code de la partie</Form.Label>
                    <Form.Control
                        name="roomCode"
                        type="text"
                        placeholder="monCodeSecret"
                        value={roomCode}
                        onChange={handleInputChange} />
                    </Card.Body>
                    { displayWarningNotFound && getWarningNotFound() }
                    <Card.Footer>
                        <Button onClick={handleJoinPrivateGame}>
                            Rejoindre
                        </Button>
                    </Card.Footer>
                </Card>
            </Col>
            <Col>
                <h2>Parties Publiques</h2>
                <GameListComponent rooms={rooms}></GameListComponent>
            </Col>
        </Row>
    )
}