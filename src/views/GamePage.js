import { useEffect, useState } from 'react'

import { useDispatch, useSelector } from 'react-redux'

import { selectCurrentRoom, selectCurrentScore, selectTimer, selectWord } from '../core/selectors';

import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Form from 'react-bootstrap/Form'

import { FcAlarmClock } from 'react-icons/fc'

import { LeaderboardShortComponent } from "../components/leaderboard/LeaderboardShortComponent"
import { DrawingComponent } from "../components/drawing/DrawingComponent"
import { ChatComponent } from "../components/chat/ChatComponent"
import { GameResultsComponent } from '../components/game/GameResultsComponent'
import { GameTitleComponent } from '../components/game/GameTitleComponent'
import UserService from '../services/UserService';
import { setCurrentScore } from '../core/actions';

export const GamePage = () => {

    const currentRoom = useSelector(selectCurrentRoom)
    const word = useSelector(selectWord)
    const timer = useSelector(selectTimer)
    const scores = useSelector(selectCurrentScore)
    const dispatch = useDispatch()

    const instruction_1 = "Manche 1 : Dessine-moi"
    const timeLeft_1 = "15"

    const instruction_2 = "Manche 1 termineé : Voici vos dessins ludiques et amusants de moutons"
    const timeLeft_2 = "0"

    const instruction_3 = "Partie terminée : Voici le classement"

    const hardCodedGame = { id: "789", title: "Les excités du pinceau" }

    const [context, setContext] = useState("game")
    const [instruction, setInstruction] = useState(instruction_1)
    const [timeLeft, setTimeLeft] = useState(timeLeft_1)
    useEffect(() => {
        let playersListTmp = [];
        if(currentRoom.players.length !== 0){   
            currentRoom.players.forEach(function(playerId) {
                UserService.getInstance().getUser(playerId).then((response) => {
                    response.json().then((player) => {
                        if(response.status == 200 && response.length !== 0) {
                            playersListTmp.push({
                                ...player,
                                rank : "#",
                                points: 0
                            })
                        }
                    })
                })
            })
        }
        dispatch(setCurrentScore(playersListTmp))
    }, [])

    const handleChangeContext = (event) => {
        const value = event.target.value
        setContext(value)

        if(value === "game") {
            setInstruction(instruction_1)
            setTimeLeft(timeLeft_1)
        } else if(value === "results") {
            setInstruction(instruction_2)
            setTimeLeft(timeLeft_2)
        } else if(value === "endResults") {
            setInstruction(instruction_3)
            setTimeLeft(timeLeft_2)
        }
    }

    return (
        <Row>
            <Col xs={3}>
                <LeaderboardShortComponent></LeaderboardShortComponent>
            </Col>
            <Col>
                <Form.Select onChange={handleChangeContext} className="m-3">
                    <option value="game">Jeu</option>
                    <option value="results">Fin de manche</option>
                    <option value="endResults">Fin de partie</option>
                </Form.Select>

                <GameTitleComponent room={currentRoom}></GameTitleComponent>

                <div>
                    <h4>{instruction} {word}</h4>
                    { context !== "endResults" && <h5><FcAlarmClock />&nbsp;{timer}s</h5> }
                </div>

                { context === "game" && <DrawingComponent></DrawingComponent> }
                { context === "results" && <GameResultsComponent></GameResultsComponent> }
                { context === "endResults" && <LeaderboardShortComponent></LeaderboardShortComponent> }
            </Col>
            <Col xs={3}>
                <ChatComponent></ChatComponent>
            </Col>
        </Row>
    )
}