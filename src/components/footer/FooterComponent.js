import { RiGitlabFill } from 'react-icons/ri';

export const FooterComponent = () => {
    return (
        <div className="mb-3">
            <hr/>
            <div>
                CPE - Projet Final
            </div>
            <div>
                GALDEANO Nasri | GOUY Quentin | PROTIERE Axel | VEBER Vincent | VERGNON Corentin
            </div>
            <div>
                <RiGitlabFill size={20} />
                &nbsp;
                <a href="https://gitlab.com/Gouyq1/front-drawing" target="_blank" rel="noreferrer">GitLab</a>
                &nbsp;
                <RiGitlabFill size={20} />
            </div>
        </div>
    )
}