export const GameTitleComponent = ({room}) => {
    return (
        <div>
            <h2>Partie #{room.id}</h2>
            <h3>{room.roomName}</h3>
            <hr/>
        </div>
    )
}