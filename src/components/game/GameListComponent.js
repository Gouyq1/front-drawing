import Stack from 'react-bootstrap/Stack'

import { GameListRowComponent } from './GameListRowComponent'

export const GameListComponent = ({rooms}) => {
    return (
        <Stack gap={3}>
            { rooms.length !== 0 ? rooms.map(room => <GameListRowComponent room={room} key={room.id} />) : <p>Il n'y a pas de partie...</p> }
        </Stack>
    )
}