import CardGroup from 'react-bootstrap/CardGroup'

import { GameResultsRowComponent } from './GameResultsRowComponent'

export const GameResultsComponent = () => {

    const hardCodedResults = [
        { player: "Jane Doe", image: "https://findicons.com/files/icons/2770/ios_7_icons/256/pig.png", accuracy: 95 },
        { player: "John Smith", image: "https://img.icons8.com/cotton/2x/fa314a/sleeping-baby.png", accuracy: 93 },
        { player: "John Doe", image: "http://www.collectif-murmuration.org/wp-content/uploads/2020/12/bird_icon.png", accuracy: 89 }
    ]

    return (
        <CardGroup>
            { hardCodedResults !== undefined ? 
                hardCodedResults.map(result => <GameResultsRowComponent result={result} key={result.player} />) : '' }
        </CardGroup>
    )
}