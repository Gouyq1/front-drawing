import Card from 'react-bootstrap/Card'

export const GameResultsRowComponent = ({result}) => {
    return (
        <Card>
            <Card.Header>
                {result.player}
            </Card.Header>
            <Card.Img src={result.image}/>
            <Card.Footer>
                {result.accuracy}&nbsp;%
            </Card.Footer>
        </Card>
    )
}