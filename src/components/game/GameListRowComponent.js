import { useDispatch, useSelector } from 'react-redux'
import { setCurrentRoom } from '../../core/actions';

import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Button from 'react-bootstrap/Button'
import { useHistory } from 'react-router';
import RoomService from '../../services/RoomService';
import { selectUser } from '../../core/selectors';

export const GameListRowComponent = ({room}) => {

    const dispatch = useDispatch()
    const user = useSelector(selectUser)
    const history = useHistory();

    const handleJoinGame = () => {
        RoomService.getInstance().joinRoom(room.id, user.id);
        dispatch(setCurrentRoom(room))
        history.push('lobby')
    }

    return (
        <Row>
            <Col>
                <div className="ms-auto">{room.roomName}</div>
            </Col>
            <Col xs={2}>
                <hr/>
            </Col>
            <Col xs={2}>
                <Button onClick={handleJoinGame}>Rejoindre</Button>
            </Col>
        </Row>
    )
}