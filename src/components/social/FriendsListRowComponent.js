import { FcBusinessman, FcCheckmark, FcCancel } from 'react-icons/fc'
import { AiOutlineLink } from 'react-icons/ai'

import Button from 'react-bootstrap/Button'
import { Nav } from 'react-bootstrap'
import SocialService from '../../services/SocialService'

import { useSelector } from 'react-redux'
import { selectUser } from '../../core/selectors'

export const FriendsListRowComponent = ({friend}) => {

    const user = useSelector(selectUser)

    const handleChat = () => {
        console.log("Chat with " + friend.id)
    }

    const handleDeleteFriend = () => {
        let parameters = { userId1: user.id, userId2: friend.id }
        SocialService.getInstance().deleteFriend(parameters).then((response) => {
            window.location.reload()
        })
    }

    const hrefProfile = `/profile/${friend.id}`

    return (
        <tr>
            <td>
                <Button variant='danger' onClick={handleDeleteFriend}>
                    X
                </Button>
            </td>
            <td>
                <FcBusinessman size={40} />
            </td>
            <td>
                <Nav.Link href={hrefProfile}>{friend.username}&nbsp;<AiOutlineLink /></Nav.Link>
            </td>
            <td>
                { friend.status === 'ONLINE' ? <FcCheckmark size={40} /> : <FcCancel size={40} /> }
            </td>
            <td>
                <Button onClick={handleChat} disabled>
                    Chatter
                </Button>
            </td>
        </tr>
    )
}