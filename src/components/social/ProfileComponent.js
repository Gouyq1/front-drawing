import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Container from 'react-bootstrap/Container'

import { useEffect, useState } from 'react'
import { useParams } from 'react-router'

import UserService from '../../services/UserService'
import { useSelector } from 'react-redux'
import { selectUser } from '../../core/selectors'
import { UserDisplay } from '../../model/UserDisplay'

import { useDispatch } from 'react-redux'
import { setUser } from '../../core/actions'
import SocialService from '../../services/SocialService'

import { useHistory } from 'react-router'

export const ProfileComponent = () => {

    const user = useSelector(selectUser)
    const [userName, setUserName] = useState()
    const [firstName, setFirstName] = useState()
    const [lastName, setLastName] = useState()
    const [description, setDescription] = useState()
    const [birthDate, setBirthDate] = useState()
    const [password, setPassword] = useState()
    const [isCurrentUser, setIsCurrentUser] = useState(false)
    const [isFriend, setIsFriend] = useState(false)

    const dispatch = useDispatch()

    const history = useHistory()

    const userId = useParams().id

    useEffect(() => {
        // If the current user is viewing his own profile.
        if(userId === user.id) {
            setIsCurrentUser(true)

            setFirstName(user.firstName)
            setLastName(user.lastName)
            setDescription(user.description)
            setBirthDate(user.birthDate)
            setUserName(user.username)
            setPassword(user.password)

        } else {
            UserService.getInstance().getUser(userId).then((response) => {
                // Si on veut voir le profil d'un "hardCoded".
                /*
                if(response && response.length !== 0) {
                    setFirstName(response[0].firstName)
                    setLastName(response[0].lastName)
                    setDescription(response[0].description)
                    setBirthDate(response[0].birthDate)
                    setUserName(response[0].username)
                    setPassword(response[0].password)
                }
                */

                // Si on veut voir le profil d'un vrai user.
                response.json().then((value) => {
                    let friend = new UserDisplay(value.id, value.username, value.firstName, value.lastName, value.description, value.birthDate, value.password)

                    setFirstName(friend.firstName)
                    setLastName(friend.lastName)
                    setDescription(friend.description)
                    setBirthDate(friend.birthDate)
                    setUserName(friend.username)
                    setPassword(friend.password)
                })
            })

            SocialService.getInstance().getFriends(user.id).then((response) => {
                response.json().then((value) => {
                    for(let friendTmp in value) {
                        if(friendTmp.id === userId) {
                            setIsFriend(true)
                        }
                    }
                })
            })
        }
    }, [])

    const handleSubmit = (e) => {
        e.preventDefault()
        let user = new UserDisplay(userId, userName, firstName, lastName, description, birthDate, password)

        UserService.getInstance().updateUser(user).then(function(response) {
            response.json().then(function(value) {
                if(response.status === 200) {
                    dispatch(setUser(user))
                    window.location.reload(false)
                } else {
                    //alert("The user does not exists... Please check the fields!")
                }
            })
            
        })
    }

    const handleInputChange = (event) => {
        const value = event.target.value
        const name = event.target.name

        switch(name) {
            case "firstName":
                setFirstName(value)
                break
            case "lastName":
                setLastName(value)
                break
            case "description":
                setDescription(value)
                break
            case "birthDate":
                setBirthDate(value)
                break    
            case "userName":
                setUserName(value)
                break
            case "password":
                setPassword(value)
                break
            default: 
                break
        }
    }

    const handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            handleSubmit()
        }
    }

    const handleAddFriend = () => {
        let parameters = { userId1: user.id, userId2: userId }
        SocialService.getInstance().addFriend(parameters).then((response) => {
            history.push("/friends")
        })
    }

    const handleDeleteFriend = () => {
        let parameters = { userId1: user.id, userId2: userId }
        SocialService.getInstance().deleteFriend(parameters).then((response) => {
            window.location.reload()
        })
    }

    return (
        <Container>
            { isCurrentUser && <h2>Profil de { user.firstName } { user.lastName }</h2> }
            { !isCurrentUser && <h2>Profil de { firstName } { lastName }</h2>}
            <Form onSubmit={handleSubmit} onKeyPress={handleKeyPress}>
                <Form.Group className="m-3">
                    <Form.Label>
                        Prénom
                    </Form.Label>
                    <Form.Control
                        disabled={!isCurrentUser}
                        name="firstName"
                        type="text"
                        placeholder="Prénom"
                        required
                        value={firstName}
                        onChange={handleInputChange} />
                </Form.Group>
                <Form.Group className="m-3">
                    <Form.Label>
                        Nom
                    </Form.Label>
                    <Form.Control
                        disabled={!isCurrentUser}
                        name="lastName"
                        type="text"
                        placeholder="Nom"
                        required
                        value={lastName}
                        onChange={handleInputChange} />
                </Form.Group>
                <Form.Group className="m-3">
                    <Form.Label>
                        Description
                    </Form.Label>
                    <Form.Control
                        disabled={!isCurrentUser}
                        name="description"
                        type="text"
                        placeholder="Description"
                        value={description}
                        onChange={handleInputChange} />
                </Form.Group>
                <Form.Group className="m-3">
                    <Form.Label>
                        Date de naissance
                    </Form.Label>
                    <Form.Control
                        disabled={!isCurrentUser}
                        name="birthDate"
                        type="date"
                        value={birthDate}
                        onChange={handleInputChange} />
                </Form.Group>
                <Form.Group className="m-3">
                    <Form.Label>
                        Nom d'utilisateur
                    </Form.Label>
                    <Form.Control
                        disabled={!isCurrentUser}
                        name="userName"
                        type="text"
                        placeholder="Nom d'utilisateur"
                        required
                        value={userName}
                        onChange={handleInputChange}/>
                </Form.Group>
                { isCurrentUser && (
                    <Form.Group className="m-3">
                        <Form.Label>
                            Mot de passe
                        </Form.Label>
                        <Form.Control
                            name="password"
                            type="password"
                            placeholder="Mot de passe"
                            required
                            value={password}
                            onChange={handleInputChange} />
                    </Form.Group>
                )}
                { isCurrentUser && <Button className="m-3" type="submit">Enregistrer</Button> }
                { (!isCurrentUser && !isFriend) && <Button className="m-3" onClick={handleAddFriend}>Ajouter en ami</Button> }
                { (!isCurrentUser && isFriend) && <Button className="m-3" onClick={handleDeleteFriend}>Supprimer des amis</Button> }
            </Form>
        </Container>
    )
}