import Table from 'react-bootstrap/Table'

import { FriendsListRowComponent } from './FriendsListRowComponent'

import { UserDisplay } from '../../model/UserDisplay'
import { useEffect } from 'react'
import SocialService from '../../services/SocialService'

import { useSelector } from 'react-redux'

import { selectUser } from '../../core/selectors'

import { useState } from 'react'

export const FriendsListComponent = () => {

    const user = useSelector(selectUser)

    const [friends, setFriends] = useState()

    useEffect(() => {
        SocialService.getInstance().getFriends(user.id).then((response) => {
            response.json().then((value) => {
                setFriends(value)
            })
        })
    }, [])

    return (
        <div>
            <h2>Mes amis rigolos</h2>
            <Table striped bordered hover className="align-middle">
                <thead>
                    <tr>
                        <th></th>
                        <th>Avatar</th>
                        <th>Nom</th>
                        <th>En ligne</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                { (friends !== undefined && friends.length !== 0) ?
                    friends.map(friend => <FriendsListRowComponent friend={friend} key={friend.id} />) : <tr></tr> }
                </tbody>
            </Table>
        </div>
    )
}