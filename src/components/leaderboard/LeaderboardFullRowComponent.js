import { AiOutlineLink } from 'react-icons/ai'

import Nav from 'react-bootstrap/Nav'

export const LeaderboardFullRowComponent = ({row}) => {

    const hrefProfile = `/profile/${row.id}`

    return (
        <tr>
            <td>{row.rank}</td>
            <td><Nav.Link href={hrefProfile}>{row.player}&nbsp;<AiOutlineLink /></Nav.Link></td>
            <td>{row.accuracy}</td>
            <td>{row.numberOfGames}</td>
        </tr>
    )
}