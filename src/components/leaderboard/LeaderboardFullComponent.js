import { useEffect, useState } from 'react'

import Form from 'react-bootstrap/Form'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Table from 'react-bootstrap/Table'

import { LeaderboardFullRowComponent } from './LeaderboardFullRowComponent'
import SocialService from '../../services/SocialService'

import { useSelector } from 'react-redux'
import { selectUser } from '../../core/selectors'

export const LeaderboardFullComponent = () => {

    const user = useSelector(selectUser)

    const handleFilterLeaderboard = (event) => {
        if(event.target.value === "global") {
            setRows(hardCodedRows_global)
            //setRows(rowsGlobal)
        } else if(event.target.value === "friends") {
            setRows(hardCodedRows_friends)
            //setRows(rowsFriends)
        }
    }

    const hardCodedRows_global = [
        { rank: 1, player: "ÉricTremblay", id: 5, accuracy: 96, numberOfGames: 42 },
        { rank: 2, player: "NinetteChrétien", id: 4, accuracy: 92, numberOfGames: 37 },
        { rank: 3, player: "RémyDucharme", id: 1, accuracy: 83, numberOfGames: 31 },
        { rank: 4, player: "EstelleAdler", id: 2, accuracy: 74, numberOfGames: 24 },
        { rank: 5, player: "HilaireAustin", id: 3, accuracy: 33, numberOfGames: 9 }
    ]

    const hardCodedRows_friends = [
        { rank: 1, player: "ÉricTremblay", id: 5, accuracy: 96, numberOfGames: 42 },
        { rank: 2, player: "RémyDucharme", id: 1, accuracy: 83, numberOfGames: 31 },
        { rank: 3, player: "HilaireAustin", id: 3, accuracy: 33, numberOfGames: 9 }
    ]

    const [rows, setRows] = useState(hardCodedRows_global)
    const [rowsGlobal, setRowsGlobal] = useState()
    const [rowsFriends, setRowsFriends] = useState()

    useEffect(() => {
        SocialService.getInstance().getGlobalRanking().then((response) => {
            response.json().then((value) => {
                setRowsGlobal(value)
            })
        })

        SocialService.getInstance().getFriendsRanking(user.id).then((response) => {
            response.json().then((value) => {
                setRowsFriends(value)
            })
        })

        //setRows(rowsGlobal)
    }, [])

    return (
        <div>
            <Row className="m-3">
                <Col><h2>Classement</h2></Col>
                <Col xs={2}>
                    <Form.Select onChange={handleFilterLeaderboard}>
                        <option value="global">Général</option>
                        <option value="friends">Amis</option>
                    </Form.Select>
                </Col>
            </Row>
            <Table striped bordered hover className="align-middle">
                <thead>
                    <tr>
                        <th>Rang</th>
                        <th>Joueur</th> 
                        <th>Précision moyenne</th>
                        <th>Nombre de parties</th>
                    </tr>
                </thead>
                <tbody>
                    { (rows !== undefined && rows.length !== 0) ?
                        rows.map(row => <LeaderboardFullRowComponent row={row} key={row.rank} />) : <tr></tr> }
                </tbody>
            </Table>
        </div>
    )
}