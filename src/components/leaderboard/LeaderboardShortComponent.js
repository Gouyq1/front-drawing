import { useEffect, useState } from 'react'
import Table from 'react-bootstrap/Table'
import { useSelector } from 'react-redux'
import { selectCurrentRoom, selectCurrentScore } from '../../core/selectors'
import UserService from '../../services/UserService'

import { LeaderboardShortRowComponent } from './LeaderboardShortRowComponent'

export const LeaderboardShortComponent = () => {

    const hardCodedRows = [
        { rank: 1, points: 500, player: "RémyDucharme", id: 1 },
        { rank: 2, points: 420, player: "EstelleAdler", id: 2 },
        { rank: 3, points: 69, player: "HilaireAustin", id: 3 }
    ]

    const currentRoom = useSelector(selectCurrentRoom)
    const currentScore = useSelector(selectCurrentScore)
    const [playersList, setPlayersList] = useState([]);

    return (
        <div>
            <h3>Classement</h3>
            <Table striped bordered hover className="align-middle">
                <thead>
                    <tr>
                        <th>Rang</th>
                        <th>Points</th>
                        <th>Joueur</th>
                    </tr>
                </thead>
                <tbody>
                    { currentScore.map(row => <LeaderboardShortRowComponent row={row} key={row.id} />) }
                </tbody>
            </Table>
        </div>
    )
}