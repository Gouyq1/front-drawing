import Nav from 'react-bootstrap/Nav'

import { AiOutlineLink } from 'react-icons/ai'

export const LeaderboardShortRowComponent = ({row}) => {

    const hrefProfile = `/profile/${row.id}`

    return (
        <tr>
            <td>{row.rank}</td>
            <td>{row.points}</td>
            <td>
                <Nav.Link href={hrefProfile} target="_blank" >
                    {row.username}&nbsp;<AiOutlineLink />
                </Nav.Link>
            </td>
        </tr>
    )
}