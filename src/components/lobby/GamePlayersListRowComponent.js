import { AiOutlineLink } from 'react-icons/ai'

import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Nav from 'react-bootstrap/Nav'
import Button from 'react-bootstrap/Button'

import { useSelector } from 'react-redux'

import { selectCurrentRoom, selectUser } from '../../core/selectors';
import RoomService from '../../services/RoomService'

export const GamePlayersListRowComponent = ({player}) => {

    const hrefProfile = `/profile/${player.id}`

    const room = useSelector(selectCurrentRoom)
    const user = useSelector(selectUser)

    const handleKickPlayer = () => {
        RoomService.getInstance().kickUser(room.id, player.id);
    }

    return (
        <Row>
            <Col>
                <Nav.Link className="ms-auto" href={hrefProfile} target="_blank">{player.username}&nbsp;<AiOutlineLink /></Nav.Link>
            </Col>
            <Col xs={3}>
                <hr/>
            </Col>
            <Col xs={3}>
            { room.roomOwner == user.id ?
                <Button
                    variant="outline-danger"
                    disabled={player.id == user.id}
                    onClick={handleKickPlayer}>
                    Expulser
                </Button> : ""}
            </Col>
        </Row>
    )
}