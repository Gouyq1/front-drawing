import { useEffect, useState } from 'react'

import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import { RoomService } from '../../services/RoomService'
import { useHistory } from 'react-router'

import { useDispatch, useSelector } from 'react-redux'

import { selectCurrentRoom, selectUser } from '../../core/selectors';
import { setCurrentRoom } from '../../core/actions'

export const GameConfigurationComponent = ({context}) => {

    const room = useSelector(selectCurrentRoom)
    const user = useSelector(selectUser)

    const [roomName, setRoomName] = useState("Partie des amis rigolos pour se régaler la chique")
    const [nbJoueurs, setNbJoueurs] = useState(5)
    const [nbManches, setNbManches] = useState(5)
    const [nbPlayer, setNbPlayer] = useState(5)
    const [tempsDessin, setTempsDessin] = useState(30)
    const [isPrivate, setIsPrivate] = useState(false)
    const [roomCode, setRoomCode] = useState("")

    const [contextIsCreation, setContextIsCreation] = useState(false)

    const history = useHistory()
    const dispatch = useDispatch()

    const handleSubmit = (e) => {
        e.preventDefault()

        let parameters = {
            roomName: roomName,
            roomOwner: user.id,
            privateRoom: isPrivate,
            rounds: nbManches,
            maxPlayers: nbPlayer,
            timeToDraw: tempsDessin,
            code: roomCode
        }

        RoomService.getInstance().createRoom(user.id, parameters).then(function(response) {
            response.json().then((room) => {
                if(response.status == 200){
                    RoomService.getInstance().joinRoom(room.id, user.id);
                    dispatch(setCurrentRoom(room))
                    history.push('lobby')
                }
            })
        })
    }

    const handleInputChange = (event) => {
        const name = event.target.name
        const value = event.target.value

        switch(name) {
            case "roomName":
                setRoomName(value)
                break
            case "nbJoueurs":
                setNbJoueurs(value)
                break
            case "nbManches":
                setNbManches(value)
                break
            case "nbPlayer":
                setNbPlayer(value)
                break
            case "tempsDessin":
                setTempsDessin(value)
                break
            case "isPrivate":
                setIsPrivate(!isPrivate)
                break
            case "roomCode":
                setRoomCode(value)
                break
            default: 
                break;
        }
    }
    
    useEffect(() => {
        setRoomName(room.roomName)
        setNbJoueurs(room.maxPlayers)
        setNbManches(room.rounds)
        setTempsDessin(room.timeToDraw)
        setIsPrivate(room.privateRoom)
        setRoomCode(room.code)

        // Si on est en train de créer la partie, on peut éditer les champs du formulaire.
        if(context === 'lobbyParameter') {
            setContextIsCreation(true)
        } else {
            // Sinon, on considère que ce n'est que de l'affichage.
            setContextIsCreation(false)
        }
    }, [])

    return (
        <Form onSubmit={handleSubmit}>
            <h2>Paramètres</h2>
            <Form.Group className="m-3">
                <Form.Label>
                    Nom de la partie
                </Form.Label>
                <Form.Control
                    disabled={!contextIsCreation}
                    name="roomName"
                    type="text"
                    placeholder="Partie des amis rigolos pour se régaler la chique"
                    required
                    value={roomName}
                    onChange={handleInputChange} />
            </Form.Group>
            <Form.Group className="m-3">
                <Form.Label>
                    Nombre de joueurs max
                </Form.Label>
                <Form.Control
                    disabled={!contextIsCreation}
                    name="nbJoueurs"
                    type="text"
                    placeholder="5"
                    required
                    pattern="[0-9]*"
                    value={nbJoueurs}
                    onChange={handleInputChange} />
            </Form.Group>
            <Form.Group className="m-3">
                <Form.Label>
                    Nombre de manches
                </Form.Label>
                <Form.Control
                    disabled={!contextIsCreation}
                    name="nbManches"
                    type="text"
                    placeholder="5"
                    required
                    pattern="[0-9]*"
                    value={nbManches}
                    onChange={handleInputChange} />
            </Form.Group>
            <Form.Group className="m-3">
                <Form.Label>
                    Nombre de joueurs
                </Form.Label>
                <Form.Control
                    disabled={!contextIsCreation}
                    name="nbPlayer"
                    type="text"
                    placeholder="5"
                    required
                    pattern="[0-9]*"
                    value={nbPlayer}
                    onChange={handleInputChange} />
            </Form.Group>
            <Form.Group className="m-3">
                <Form.Label>
                    Temps pour dessiner
                </Form.Label>
                <Form.Control
                    disabled={!contextIsCreation}
                    name="tempsDessin"
                    type="text"
                    placeholder="30"
                    required
                    pattern="[0-9]*"
                    value={tempsDessin}
                    onChange={handleInputChange} />
            </Form.Group>
            <Form.Group className="m-3">
                <Form.Label>
                    Partie privée
                </Form.Label>
                <Form.Check
                    disabled={!contextIsCreation}
                    name="isPrivate"
                    id="isPrivate"
                    type="switch"
                    required
                    checked={isPrivate}
                    onChange={handleInputChange} />
            </Form.Group>
            <Form.Group className="m-3">
                <Form.Label>
                    Code (pour rejoindre directement la partie)
                </Form.Label>
                <Form.Control
                    disabled={!contextIsCreation}
                    name="roomCode"
                    type="text"
                    placeholder="monCodeSecret"
                    required
                    value={roomCode}
                    onChange={handleInputChange} />
            </Form.Group>
            { contextIsCreation && (
                <Button
                    className="m-3"
                    variant="primary"
                    onClick={handleSubmit}>
                    Créer
                </Button>
            )}
        </Form>
    )
}