import Stack from 'react-bootstrap/Stack'

import { GamePlayersListRowComponent } from './GamePlayersListRowComponent'

import { UserService } from '../../services/UserService'

import { useEffect, useState } from 'react'

export const GamePlayersListComponent = ({playersIds}) => {

    const [playersList, setPlayersList] = useState([])

    useEffect(() => {
        if(playersIds.length !== 0){
            playersIds.forEach(function(playerId) {
                UserService.getInstance().getUser(playerId).then((response) => {
                    response.json().then((player) => {
                        if(response.status == 200 && response.length !== 0) {
                            let playersListTmp = playersList
                            playersListTmp.push(player)
                            setPlayersList([...playersListTmp])
                        }
                    })
                    
                })
            })
        }
        
    }, [])

    return (
        <div>
            <h4>Liste des joueurs</h4>
            <Stack gap={3}>
                { playersList.length !== 0 ? playersList.map(player => <GamePlayersListRowComponent player={player} key={player.id} />) : <p>Il n'y a aucun joueur...</p> }
            </Stack>
            <hr/>
        </div>
    )
}