import Toast from 'react-bootstrap/Toast'

export const ChatMessageComponent = ({message}) => {
    return (
        <Toast className="w-100">
            <Toast.Header closeButton={false}>
                <strong className="me-auto">{message.player}</strong>
                <small>{message.time}</small>
            </Toast.Header>
            <Toast.Body>{message.message}</Toast.Body>
        </Toast>
    )
}