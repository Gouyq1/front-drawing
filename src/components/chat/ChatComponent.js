import { useState } from 'react'

import ToastContainer from 'react-bootstrap/ToastContainer'
import InputGroup from 'react-bootstrap/InputGroup'
import FormControl from 'react-bootstrap/FormControl'
import Button from 'react-bootstrap/Button'

import { ChatMessageComponent } from './ChatMessageComponent'

export const ChatComponent = () => {

    const [messageInputValue, setMessageInputValue] = useState()

    const hardCodedMessages = [
        { player: "John Doe", time: "14:45", message: "C'est moi qui vait gagner !" },
        { player: "Jane Doe", time: "14:46", message: "Bin non c'est moi !" },
        { player: "John Smith", time: "14:48", message: "C'est ici qu'on se régale la chique ?" }
    ]

    const handleChangeMessage = (event) => {
        setMessageInputValue(event.target.value)
    }

    const handleSend = (event) => {
        console.log(event.target.value)
    }

    return (
        <div>
            <h2>Chat</h2>
            <div className="m-3">
                <ToastContainer style={{ width: "unset" }}>
                    { hardCodedMessages.map(message => <ChatMessageComponent message={message} key={message.player} />) }
                </ToastContainer>
                <InputGroup className="mt-3">
                    <FormControl placeholder="Message" onChange={handleChangeMessage}>
                    </FormControl>
                    <Button variant="secondary" onClick={handleSend} disabled>
                        Envoyer
                    </Button>
                </InputGroup>
            </div>
        </div>
    )
}