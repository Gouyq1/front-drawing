import React from 'react'
import { useCanvas } from './CanvasContext'
import Button from 'react-bootstrap/Button'

export const SaveCanvasButton = () => {
  const { saveCanvas } = useCanvas()

  return <Button onClick={saveCanvas} variant="outline-primary">Envoyer</Button>
}