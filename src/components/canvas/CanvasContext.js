import React, { useContext, useRef, useState } from "react";
import { selectCurrentRoom, selectUser, selectWord } from "../../core/selectors";
import GamePythonService from "../../services/GamePythonService";

import { setCurrentScore, setLoading } from '../../core/actions'

import { useDispatch, useSelector } from 'react-redux'

const CanvasContext = React.createContext();

export const CanvasProvider = ({ children }) => {
  const [isDrawing, setIsDrawing] = useState(false)
  const canvasRef = useRef(null);
  const contextRef = useRef(null);

  const room = useSelector(selectCurrentRoom);
  const user = useSelector(selectUser);
  const word = useSelector(selectWord)

  const dispatch = useDispatch()

  const prepareCanvas = () => {
    const canvas = canvasRef.current
    var sketch = document.querySelector('#sketch');
    var sketch_style = getComputedStyle(sketch);
    
    canvas.width = parseInt(sketch_style.getPropertyValue('width'));
    canvas.height = parseInt(sketch_style.getPropertyValue('height'))

    const context = canvas.getContext("2d")
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.lineCap = "round";
    context.strokeStyle = "black";
    context.lineWidth = 10;
    contextRef.current = context;
  };

  const startDrawing = ({ nativeEvent }) => {
    console.log(nativeEvent)
    const { offsetX, offsetY } = nativeEvent;
    contextRef.current.beginPath();
    contextRef.current.moveTo(offsetX, offsetY);
    setIsDrawing(true);
  };

  const finishDrawing = () => {
    contextRef.current.closePath();
    setIsDrawing(false);
  };

  const draw = ({ nativeEvent }) => {
    if (!isDrawing) {
      return;
    }
    const { offsetX, offsetY } = nativeEvent;
    contextRef.current.lineTo(offsetX, offsetY);
    contextRef.current.stroke();
  };

  const clearCanvas = () => {
    const canvas = canvasRef.current;
    const context = canvas.getContext("2d")
    context.clearRect(0, 0, 280, 280);
    //context.fillRect(0, 0, canvas.width, canvas.height)
    dispatch(setLoading(false))
  }

  const saveCanvas = () => {

    dispatch(setLoading(true))

    const canvas = canvasRef.current;
    const context = canvas.getContext("2d")
    var img = context.getImageData(0, 0, canvas.width, canvas.height); 
    var imgArray  = img.data
    var reduceImgArray = []
    for(var i = 0; i < canvas.height * canvas.width; i++){
      reduceImgArray[i] = imgArray[i*4+3]; 
    }
    var doubleImgArray = [];
    for(var i = 0; i<canvas.width; i++){
      doubleImgArray[i] = []
      for(var j = 0; j<canvas.height; j++){
        doubleImgArray[i][j] = reduceImgArray[i*canvas.height+j] /255
      }
    }
    var reducedImgArray = []; 

    for(var i = 0; i<28; i++){
      reducedImgArray[i] = []
      for (var j = 0; j<28 ;j++){
        var totalValue = 0
        for (var k = 0; k<10; k++){
          for(var l = 0; l<10; l++){
            totalValue += doubleImgArray[i*10+k][j*10+l]
          }
        }
        totalValue = totalValue/100
        reducedImgArray[i][j] = totalValue
      }
    }

    let data = {
      class: word,
      playerId: user.id,
      gameId: room.id,
      roundId: "32432dzdzff3232",
      image: reducedImgArray
    }

    var result = GamePythonService.getInstance().sendImageData(data); 
    result.then((response)=>{
      response.json().then((value)=>{
        dispatch(setLoading(false))
      })
    });
  }


  return (
    <CanvasContext.Provider
      value={{
        canvasRef,
        contextRef,
        prepareCanvas,
        startDrawing,
        finishDrawing,
        clearCanvas,
        draw,
        saveCanvas,
      }}
    >
      {children}
    </CanvasContext.Provider>
  );
};

export const useCanvas = () => useContext(CanvasContext);
