import React from 'react'
import { useCanvas } from './CanvasContext'
import Button from 'react-bootstrap/Button'

export const ClearCanvasButton = () => {
  const { clearCanvas } = useCanvas()

  return <Button onClick={clearCanvas} variant="outline-danger">Effacer</Button>
}