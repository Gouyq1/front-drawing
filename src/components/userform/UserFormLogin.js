import { useRef, useState } from 'react'
import { UserService } from '../../services/UserService'
import { UserLogin } from '../../model/UserLogin'
import { UserDisplay } from '../../model/UserDisplay'

import { useDispatch } from 'react-redux'

import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import { setUser, setIsLogged } from '../../core/actions'


export const UserFormLogin = () => {

    const ref = useRef(null)
    const [login, setLogin] = useState()
    const [password, setPassword] = useState()
    const dispatch = useDispatch()

    const handleSubmit = (e) => {
        e.preventDefault()
        let user = new UserLogin(login, password)

        UserService.getInstance().authenticate(user).then(function(response) {
            console.log(response);
            response.json().then(function(value) {
                console.log(value)
                if(response.status === 200) {
                    let currentUser = new UserDisplay(value.id, value.username,
                        value.firstName, value.lastName, value.description,
                        value.birthDate, value.password)
                    dispatch(setUser(currentUser))
                    dispatch(setIsLogged(true))
                    console.log('Action connexion')
                } else {
                    alert("The user does not exists... Please check the fields!")
                }
            })
        })
    }

    const handleInputChange = (event) => {
        const value = event.target.value
        const name = event.target.name

        switch(name) {
            case "login":
                setLogin(value)
                break
            case "password":
                setPassword(value)
                break
            default:
                break
        }
    }

    const handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            handleSubmit()
        }
    }

    return (
        <Form onSubmit={handleSubmit} onKeyPress={handleKeyPress} ref={ref}>
            <Form.Group className="m-3">
                <Form.Label>
                    Nom d'utilisateur
                </Form.Label>
                <Form.Control
                    name="login"
                    type="text"
                    placeholder="Nom d'utilisateur"
                    required
                    value={login}
                    onChange={handleInputChange}/>
            </Form.Group>
            <Form.Group className="m-3">
                <Form.Label>
                    Mot de passe
                </Form.Label>
                <Form.Control
                    name="password"
                    type="password"
                    placeholder="Mot de passe"
                    required
                    value={password}
                    onChange={handleInputChange} />
            </Form.Group>
            <Button
                className="m-3"
                variant="primary"
                onClick={handleSubmit} >
                Se connecter
            </Button>
        </Form>
    )
}