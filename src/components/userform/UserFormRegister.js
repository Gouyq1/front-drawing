import { useState } from "react"
import UserService from "../../services/UserService"
import { UserRegister } from "../../model/UserRegister"

import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import { useHistory } from "react-router"


export const UserFormRegister = (props) => {

    const history = useHistory()
    const [userName, setUserName] = useState(props.lastName)
    const [firstName, setFirstName] = useState(props.surName)
    const [lastName, setLastName] = useState(props.lastName)
    
    const [description, setDescription] = useState(props.description)
    const [birthday, setBirthday] = useState(props.description)
    const [password, setPassword] = useState(props.password)

    const handleSubmit = (e) => {
        e.preventDefault()
        let user = new UserRegister(userName, firstName, lastName, description, birthday, password)
        console.log(user);
        UserService.getInstance().addUser(user).then(function(response) {
            if(response.status === 200) {
                history.push('login')
            } else {
                alert("Cela n'a pas fonctionné...")
            }
        })
    }

    const handleInputChange = (event) => {
        const value = event.target.value
        const name = event.target.name

        switch(name) {
            case "firstName":
                setFirstName(value)
                break
            case "lastName":
                setLastName(value)
                break
            case "description":
                setDescription(value)
                break
            case "birthday":
                setBirthday(value)
                break    
            case "userName":
                setUserName(value)
                break
            case "password":
                setPassword(value)
                break
            default: 
                break;
        }
    }

    const handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            handleSubmit()
        }
    }

    return (
        <Form onSubmit={handleSubmit} onKeyPress={handleKeyPress}>
            <Form.Group className="m-3">
                <Form.Label>
                    Prénom
                </Form.Label>
                <Form.Control
                    name="firstName"
                    type="text"
                    placeholder="Prénom"
                    required
                    value={firstName}
                    onChange={handleInputChange} />
            </Form.Group>
            <Form.Group className="m-3">
                <Form.Label>
                    Nom
                </Form.Label>
                <Form.Control
                    name="lastName"
                    type="text"
                    placeholder="Nom"
                    required
                    value={lastName}
                    onChange={handleInputChange} />
            </Form.Group>
            <Form.Group className="m-3">
                <Form.Label>
                    Description
                </Form.Label>
                <Form.Control
                    name="description"
                    type="text"
                    placeholder="Description"
                    value={description}
                    onChange={handleInputChange} />
            </Form.Group>
            <Form.Group className="m-3">
                <Form.Label>
                    Date de naissance
                </Form.Label>
                <Form.Control
                    name="birthday"
                    type="date"
                    placeholder="01/01/1990"
                    value={birthday}
                    onChange={handleInputChange} />
            </Form.Group>
            <Form.Group className="m-3">
                <Form.Label>
                    Nom d'utilisateur
                </Form.Label>
                <Form.Control
                    name="userName"
                    type="text"
                    placeholder="Nom d'utilisateur"
                    required
                    value={userName}
                    onChange={handleInputChange}/>
            </Form.Group>
            <Form.Group className="m-3">
                <Form.Label>
                    Mot de passe
                </Form.Label>
                <Form.Control
                    name="password"
                    type="password"
                    placeholder="Mot de passe"
                    required
                    value={password}
                    onChange={handleInputChange} />
            </Form.Group>
            <Button
                className="m-3"
                variant="primary"
                type="submit">
                Créer le compte
            </Button>
        </Form>
    )
}