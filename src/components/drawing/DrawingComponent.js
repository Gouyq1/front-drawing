import { CanvasProvider, useCanvas } from '../canvas/CanvasContext';
import { Canvas } from '../canvas/Canvas';
import { ClearCanvasButton } from '../canvas/ClearCanvasButton';
import { SaveCanvasButton } from '../canvas/SaveCanvasButton';

import Stack from 'react-bootstrap/Stack'
import ButtonGroup from 'react-bootstrap/ButtonGroup'
import Container from 'react-bootstrap/Container';
import { selectLoading } from '../../core/selectors';

import { useSelector } from 'react-redux'

import Spinner from 'react-bootstrap/Spinner'

export const DrawingComponent = () => {

    const loading = useSelector(selectLoading)

    //const { imageRef } = useCanvas();
    return (
        <CanvasProvider>
            <Stack gap={3}>
                <Container className="d-flex justify-content-center">
                    <div className="border border-dark">
                        <div id="sketch">
                            <Canvas/>
                        </div>
                    </div>
                </Container>
                <Container>
                    { loading && (
                        <Spinner animation="border" role="status">
                        </Spinner>
                    )}
                </Container>
                <ButtonGroup>
                    <ClearCanvasButton/>
                    <SaveCanvasButton/>
                </ButtonGroup>
            </Stack>
        </CanvasProvider>
    )
}