import { FcPicture } from 'react-icons/fc'
import { FaUser } from 'react-icons/fa'

import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import { MenuComponent } from './MenuComponent'
import { Button } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux'
import { selectUser, selectIsLogged } from '../../core/selectors';
import { setLogoutUser } from '../../core/actions';

import { useHistory } from 'react-router';

export const HeaderComponent = () => {

    const user = useSelector(selectUser)
    const isLogin = useSelector(selectIsLogged)
    const dispatch = useDispatch()
    const history = useHistory();

    const handleLogout = () => {
        dispatch(setLogoutUser())
    }

    const handleLogin = () => {
        history.push('login')
    }

    const handleRegister = () => {
        history.push('register')
    }

    return (
        <div>
            <Row className="align-items-center">
                <Col className="mt-3">
                    <div>
                        <h1><FcPicture />&nbsp;Piction<u>IA</u>ry&nbsp;<FcPicture /></h1>
                    </div>
                    <div>
                        <b><i>Dessins ludiques et amusants pour se régaler la chique entre amis rigolos</i></b>
                    </div>
                </Col>
                <Col>
                    <MenuComponent></MenuComponent>
                </Col>
                <Col>
                    { isLogin ? (
                        <div>
                            <h1>{user.firstName} {user.lastName}&nbsp;<FaUser /></h1>
                            <Button onClick={handleLogout} variant="outline-danger">
                                Se déconnecter
                            </Button>
                        </div>
                    ) : (
                        <div>
                            <Button onClick={handleRegister}>
                                Créer un compte
                            </Button>
                            &nbsp;
                            <Button onClick={handleLogin} variant="outline-primary">
                                Se connecter
                            </Button>
                        </div>
                    )}
                </Col>
            </Row>
            <hr/>
        </div>
    )
}