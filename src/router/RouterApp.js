import { Switch } from "react-router-dom"
import { Route } from "react-router"

import { PrivateRoute } from "./PrivateRoute"
import { PublicRoute } from "./PublicRoute"

import { HomePage } from "../views/HomePage"
import { LobbyPage } from "../views/LobbyPage"
import { GamePage } from "../views/GamePage"
import { LeaderboardPage } from "../views/LeaderboardPage"
import { FriendsPage } from "../views/FriendsPage"
import { ProfilePage } from "../views/ProfilePage"
import { LoginPage } from "../views/LoginPage"
import { RegisterPage } from "../views/RegisterPage"
import { LobbyParameterPage } from "../views/LobbyParameterPage"

export const RouterApp = () => {
    return (
        <Switch>
            <PrivateRoute exact component={HomePage} path="/" />
            <PrivateRoute exact component={LobbyParameterPage} path="/lobbyParam" />
            <PrivateRoute exact component={LobbyPage} path="/lobby" />
            <PrivateRoute exact component={GamePage} path="/game" />
            <PrivateRoute exact component={LeaderboardPage} path="/leaderboard" />
            <PrivateRoute exact component={FriendsPage} path="/friends" />
            <PrivateRoute component={ProfilePage} path="/profile/:id" />
            <PublicRoute exact component={LoginPage} path="/login"/>
            <PublicRoute exact component={RegisterPage} path="/register"/>
            <Route><h1>404 Not Found</h1></Route>
        </Switch>
    );
}