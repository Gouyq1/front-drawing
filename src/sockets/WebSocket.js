import React, { createContext } from 'react'
import io from 'socket.io-client';
import { useDispatch, useSelector } from 'react-redux';
import { selectCurrentRoom, selectRooms, selectUser } from '../core/selectors';
import { setCurrentRoom, setCurrentScore, setRooms, setTimer, setWord } from '../core/actions';
import { useHistory } from 'react-router';
import UserService from '../services/UserService';
const Stomp = require('stompjs');


const WebSocketContext = createContext(null)

export { WebSocketContext }

export default ({ children }) => {
    let client;
    let ws;

    const dispatch = useDispatch();
    const rooms = useSelector(selectRooms)
    const currentRoom = useSelector(selectCurrentRoom)
    const history = useHistory();
    const user = useSelector(selectUser)

    if (!client) {
        const sock = new WebSocket('ws://localhost:8080/stomp');
        client = Stomp.over(sock);

        client.connect({}, () => {
            client.subscribe('/RL_CREATION', (data) => {
                let room = JSON.parse(data.body)
                let tempRooms = rooms
                if(tempRooms.filter(r => r.id == room.id).length != 0){
                    rooms = tempRooms.filter(r => r.id != room.id)
                }
                tempRooms.push(room)
                dispatch(setRooms([...tempRooms]));
            });

            client.subscribe('/RL_DELETED', (data) => {
                let room = JSON.parse(data.body)
                let tempRooms = rooms.filter(r => r.id != room.id)
                tempRooms.push(room)
                if(currentRoom.id == room.id){
                    dispatch(setCurrentRoom({}))
                }
                dispatch(setRooms([...tempRooms]));
            });

            client.subscribe('/RL_UPDATED', (data) => {
                let room = JSON.parse(data.body)
                let tempRooms = rooms.filter(r => r.id != room.id)
                tempRooms.push(room)
                dispatch(setRooms([...tempRooms]));
            });

            client.subscribe('/ROOM', (data) => {
                let response = JSON.parse(data.body)
                let room = response.roomInfosDTO;
                let action = response.actionRoom;
                switch(action) {
                    case "KICK":
                        console.log(room.players)
                        console.log(user.id)
                        if(room.players.filter(p => p == user.id).length == 0){
                            dispatch(setCurrentRoom({
                                id: null,
                                roomName: "",
                                roomOwner: "",
                                isPrivate: false,
                                rounds: null,
                                maxPlayers: null,
                                timeToDraw: null,
                                code: "",
                                players: []
                            }))
                            history.push("/")
                        }
                        else if(currentRoom.id == room.id){
                            dispatch(setCurrentRoom(room))
                            console.log(room)
                        }

                        
                        break;
                    case "JOIN":
                        if(currentRoom.id == room.id){
                            dispatch(setCurrentRoom(room))
                            console.log(room)
                        }
                        break;
                    case "DELETE":
                        if(currentRoom.id == room.id){
                            dispatch(setCurrentRoom({
                                id: null,
                                roomName: "",
                                roomOwner: "",
                                isPrivate: false,
                                rounds: null,
                                maxPlayers: null,
                                timeToDraw: null,
                                code: "",
                                players: []
                            }))
                            console.log(room)
                        }
                        break;
                    case "START":
                        history.push("game")
                        break;
                }
                
                
            });

            client.subscribe('/WORD', (data) => {
                let response = JSON.parse(data.body)
                console.log('WORD')
                if(currentRoom.id == response.gameId){
                    dispatch(setWord(response.word))
                }
                console.log(data)
            });

            client.subscribe('/TIMER', (data) => {
                console.log('TIMER')
                let response = JSON.parse(data.body)
                if(currentRoom.id == response.gameId){
                    dispatch(setTimer(response.timer))
                }
                console.log(data)
            });

            client.subscribe('/RANK', (data) => {
                console.log('RANK')
                let response = JSON.parse(data.body)
                if(currentRoom.id == response.gameId){
                    if(currentRoom.players.length !== 0){
                        let playersListTmp = [];
                        currentRoom.players.forEach(function(playerId) {
                            UserService.getInstance().getUser(playerId).then((res) => {
                                res.json().then((player) => {
                                    if(res.status == 200 && res.length !== 0) {
                                        Object.entries(response.globalRanking).forEach(entry => {
                                            const [key, value] = entry;
                                            if(key == playerId){
                                                playersListTmp.push({
                                                    ...player,
                                                    rank : "#",
                                                    points: value
                                                })
                                            }
                                        })
                                    }
                                })
                                
                            })
                        })
                        console.log(playersListTmp);
                        dispatch(setCurrentScore(playersListTmp))
                    }
                }
                console.log(data)
            });

        });

        ws = {
            client: client
        }
    }

    return (
        <WebSocketContext.Provider value={ws}>
            {children}
        </WebSocketContext.Provider>
    )
}