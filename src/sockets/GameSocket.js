import { Socket } from "./Socket";

export class GameSocket {

    static instance = null

    static getInstance() {
        if(this.instance == null) {
            this.instance = new GameSocket()
        }
        return this.instance
    }

    constructor(){
        this.socket = Socket.getInstance()
    }

    listenNewWord(id, cb) {
        this.socket.listen(`${id}/WORD`, cb);
    }

    listenTimeUpdate(cb) {
        this.socket.listen(`${id}/TIMER`, cb);
    }

    listenRankingUpdate(cb) {
        this.socket.listen(`${id}/RANK`, cb);
    }

    disconnect(){
        this.socket.disconnect();
    }
}