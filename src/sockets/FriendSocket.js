import { Socket } from "./Socket";

export class FriendSocket {

    static instance = null

    static getInstance() {
        if(this.instance == null) {
            this.instance = new FriendSocket()
        }
        return this.instance
    }

    constructor(){
        this.socket = Socket.getInstance()
    }


    disconnect(){
        this.socket.disconnect();
    }
}