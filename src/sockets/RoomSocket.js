import { Socket } from "./Socket";

export class RoomSocket {

    static instance = null

    static getInstance() {
        if(this.instance == null) {
            this.instance = new RoomSocket()
        }
        return this.instance
    }

    constructor(){
        this.socket = Socket.getInstance()
    }

    listenRoomCreation(cb) {
        this.socket.listen('/topic/RL_CREATION', cb);
    }

    listenRoomListDeleted(cb) {
        this.socket.listen('/topic/RL_DELETED', cb);
    }

    listenRoomListUpdated(cb) {
        this.socket.listen('/topic/RL_UPDATED', cb);
    }

    listenRoomUpdated(cb, roomId) {
        this.socket.listen(roomId, cb);
    }


    disconnect(){
        this.socket.disconnect();
    }
}