import { Service } from './Service'

export class SocialService extends Service {

    static instance = null

    static getInstance() {
        if(this.instance == null) {
            this.instance = new SocialService()
        }

        return this.instance
    }
    
    constructor() {
        // Notre serveur.
        const domain = "http://localhost:8082/api"

        super(domain)
    }

    getFriends(id) {
        const url = `friends/${id}`
        const method = "GET"

        return super.request(url, method)
    }

    addFriend(friend) {
        const url = `/request/friend`
        const method = "POST"
        const data = friend

        return super.request(url, method, data)
    }

    deleteFriend(friend) {
        const url = `/friend`
        const method = "DELETE"
        const data = friend

        return super.request(url, method, data)
    }

    getGlobalRanking() {
        const url = '/ranking/global'
        const method = 'GET'

        return super.request(url, method)
    }

    getFriendsRanking(userId) {
        const url = `/ranking/friend/${userId}`
        const method = 'GET'

        return super.request(url, method)
    }
}

export default SocialService