import { Service } from './Service'

export class RoomService extends Service {

    isTest = false

    static instance = null

    static getInstance() {
        if(this.instance == null) {
            this.instance = new RoomService()
        }

        return this.instance
    }
    
    constructor() {
        // Notre serveur.
        const domain = "http://127.0.0.1:8083/api"

        super(domain)
    }

    getAllActiveRooms() {
        const url = `rooms`
        const method = "GET"

        if(this.isTest) {
            const hardCodedGamesList = [
                {
                    id: 123,
                    roomName: "Venez comme vous êtes",
                    roomOwner: "2",
                    privateRoom: false,
                    rounds: 10,
                    maxPlayers: 10,
                    timeToDraw: 30,
                    code: "vcve",
                    players: [ "2", "3", "4" ]
                },
                {
                    id: 456,
                    roomName: "Les amis rigolos des dessins ludiques et amusants",
                    roomOwner: "5",
                    privateRoom: false,
                    rounds: 50,
                    maxPlayers: 50,
                    timeToDraw: 40,
                    code: "larddlea",
                    players: [ "5", "6", "7" ]
                },
                {
                    id: 789,
                    roomName: "Les excités du pinceau",
                    roomOwner: "8",
                    privateRoom: true,
                    rounds: 100,
                    maxPlayers: 5,
                    timeToDraw: 5,
                    code: "ledp",
                    players: [ "8", "9" ]
                }
            ]

            return Promise.resolve(hardCodedGamesList)
        }
        
        return super.request(url, method)
    }

    getRoomById(roomId) {
        const url = `room/${roomId}`
        const method = "GET"

        return super.request(url, method)
    }

    createRoom(userId, roomParameters) {
        const url = `room/${userId}`
        const method = "POST"
        const data = roomParameters

        return super.request(url, method, data)
    }

    deleteRoom(roomId){
        const url = `room/${roomId}`
        const method = "DELETE"

        return super.request(url, method)
    }

    joinRoom(roomId, userId) {
        const url = `join/${roomId}/${userId}`
        const method = "POST"

        if(this.isTest) {
            return Promise.resolve(true)
        }

        return super.request(url, method)
    }

    leaveRoom(roomId, userId) {
        const url = `leave/${roomId}/${userId}`
        const method = "POST"

        return super.request(url, method)
    }

    kickUser(roomId, userId) {
        const url = `kick/${roomId}/${userId}`
        const method = "POST"

        return super.request(url, method)
    }
}

export default RoomService