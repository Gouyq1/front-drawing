import { UserDisplay } from '../model/UserDisplay'
import { Service } from './Service'

export class UserService extends Service {

    static instance = null

    static getInstance() {
        if(this.instance == null) {
            this.instance = new UserService()
        }

        return this.instance
    }
    
    constructor() {
        // Notre serveur.
        const domain = "http://127.0.0.1:8081/api"

        super(domain)
    }

    authenticate(userLogin) {
        const url = `auth/${userLogin.username}/${userLogin.password}`
        const method = "GET"

        return super.request(url, method)
    }

    getUser(id) {
        const url = `user/${id}`
        const method = "GET"

        if(parseInt(id, 10) > 0 && parseInt(id, 10) <= 5) {

            const hardCodedUsers = [
                new UserDisplay(1, 'RémyDucharme', 'Rémy', 'Ducharme', 'Je suis Rémy Ducharme.', '1990-01-01', 'RémyDucharme'),
                new UserDisplay(2, 'EstelleAdler', 'Estelle', 'Adler', 'Je suis Estelle Adler.', '1990-01-01', 'EstelleAdler'),
                new UserDisplay(3, 'HilaireAustin', 'Hilaire', 'Austin', 'Je suis Hilaire Austin.', '1990-01-01', 'HilaireAustin'),
                new UserDisplay(4, 'NinetteChrétien', 'Ninette', 'Chrétien', 'Je suis Ninette Chrétien.', '1990-01-01', 'NinetteChrétien'),
                new UserDisplay(5, 'ÉricTremblay', 'Éric', 'Tremblay', 'Je suis Éric Tremblay.', '1990-01-01', 'ÉricTremblay')
            ]

            console.log("coucou")

            return Promise.resolve(hardCodedUsers.filter(function (user) {
                return parseInt(user.id, 10) === parseInt(id, 10)
            }))
        } else {
            return super.request(url, method)
        }
    }

    addUser(user) {
        const url = `user`
        const method = "POST"
        const data = user

        return super.request(url, method, data)
    }

    updateUser(user){
        const url = `user`
        const method = "PUT"
        const data = user

        return super.request(url, method, data)
    }

    deleteUser(id) {
        const url = `user/${id}`
        const method = "DELETE"

        return super.request(url, method)
    }
}

export default UserService