import { Service } from './Service'

export class GameService extends Service {
    
    isTest = true

    static instance = null

    static getInstance() {
        if(this.instance == null) {
            this.instance = new GameService()
        }

        return this.instance
    }
    
    constructor() {
        // Notre serveur.
        const domain = "http://localhost:8084/api"

        super(domain)
    }

    startGame(roomId) {
        const url = `start/${roomId}`
        const method = "POST"
        
        return super.request(url, method)
    }

}

export default GameService