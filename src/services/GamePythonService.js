import { Service } from './Service'

export class GamePythonService extends Service {
    
    isTest = true

    static instance = null

    static getInstance() {
        if(this.instance == null) {
            this.instance = new GamePythonService()
        }

        return this.instance
    }
    
    constructor() {
        // Notre serveur.
        const domain = "http://localhost:5000/ai"

        super(domain)
    }

    sendImageData(data) {
        const url = `predict`
        const method = "POST"
        
        return super.request(url, method, data)
    }

}

export default GamePythonService