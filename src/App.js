import { BrowserRouter } from 'react-router-dom';

import { HeaderComponent } from './components/header/HeaderComponent';
import { RouterApp } from './router/RouterApp';
import { FooterComponent } from './components/footer/FooterComponent';

import { Provider } from 'react-redux';
import { store, persistor } from './core/reducers';
import { PersistGate } from 'redux-persist/integration/react';

import WebSocketProvider from './sockets/WebSocket';


import 'bootstrap/dist/css/bootstrap.min.css';
import { Container } from 'react-bootstrap';

function App() {

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <BrowserRouter>
        <WebSocketProvider>
          <Container fluid className="text-center">
            <HeaderComponent></HeaderComponent>
            <div className="m-3">
              <RouterApp></RouterApp>
            </div>
            <FooterComponent></FooterComponent>
          </Container>
          </WebSocketProvider>
        </BrowserRouter>
      </PersistGate>
      
    </Provider>
  )
}

export default App;