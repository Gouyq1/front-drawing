import { combineReducers, createStore } from 'redux'
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'

import userReducer from './user.reducer'
import roomReducer from './room.reducer'
import roomsReducer from './rooms.reducer'
import gameReducer from './game.reducer'

const rootReducer = combineReducers({
    userState: userReducer,
    roomState: roomReducer,
    roomsState: roomsReducer,
    gameState: gameReducer
})

const persistConfig = {
    key: 'root',
    storage,
    whitelist: ['userState', 'roomState', 'roomsState', 'gameReducer']
}

const persistedReducer = persistReducer(persistConfig, rootReducer)

export const store = createStore(persistedReducer)

export const persistor = persistStore(store)