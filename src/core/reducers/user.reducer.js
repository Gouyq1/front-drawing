import { UPDATE_ISLOGGED, UPDATE_USER, LOGOUT_USER } from '../actions';
import { UserDisplay } from '../../model/UserDisplay';

const initialState = {
    user: new UserDisplay(1, 'JaneDoe', 'Jane', 'Doe', '', '01/01/1990', 'JaneDoe'),
    isLogged: false
}

const userReducer = (state = initialState, action) => {
    switch(action.type) {
        case UPDATE_ISLOGGED:
            return {
                ...state,
                isLogged: action.payload,
            }
        case UPDATE_USER:
            return {
                ...state,
                user: action.payload
            }
        case LOGOUT_USER:
            return {
                ...state,
                user: action.payload.user,
                isLogged: action.payload.isLogged
            }
        default:
            return state
    }
}

export default userReducer