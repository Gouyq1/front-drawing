import { UPDATE_CURRENTSCORE, UPDATE_LOADING, UPDATE_TIMER, UPDATE_WORD } from '../actions'

const initialState = {
    currentScore: [],
    word : "",
    timer: 15,
    loading: false
}

const gameReducer = (state = initialState, action) => {
    switch(action.type) {
        case UPDATE_CURRENTSCORE:
            return {
                ...state,
                currentScore: action.payload,
            }
        case UPDATE_TIMER:
            return {
                ...state,
                timer: action.payload,
            }
        case UPDATE_WORD:
            return {
                ...state,
                word: action.payload,
            }
        case UPDATE_LOADING:
            return {
                ...state,
                loading: action.payload,
            }
        default:
            return state
    }
}

export default gameReducer