import { UPDATE_CURRENTROOM } from '../actions'

const initialState = {
    currentRoom: {
        id: null,
        roomName: "Partie Invalide",
        roomOwner: "",
        isPrivate: false,
        rounds: null,
        maxPlayers: null,
        timeToDraw: null,
        code: "",
        players: []
    }
}

const roomReducer = (state = initialState, action) => {
    switch(action.type) {
        case UPDATE_CURRENTROOM:
            return {
                ...state,
                currentRoom: action.payload,
            }
        default:
            return state
    }
}

export default roomReducer