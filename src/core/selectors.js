export const selectIsLogged = state => state.userState.isLogged
export const selectUser = state => state.userState.user

export const selectCurrentRoom = state => state.roomState.currentRoom

export const selectRooms = state => state.roomsState.rooms
export const selectCurrentScore = state => state.gameState.currentScore
export const selectTimer = state => state.gameState.timer 
export const selectWord = state => state.gameState.word
export const selectLoading = state => state.gameState.loading
