import { UserDisplay } from '../model/UserDisplay';
export const UPDATE_ISLOGGED = 'UPDATE_ISLOGGED'
export const UPDATE_USER = 'UPDATE_USER'
export const LOGOUT_USER = 'LOGOUT_USER'

export const UPDATE_CURRENTROOM = 'UPDATE_CURRENTROOM'
export const UPDATE_ROOMS = 'UPDATE_ROOMS'

export const UPDATE_CURRENTSCORE = 'UPDATE_CURRENTSCORE'
export const UPDATE_TIMER = 'UPDATE_TIMER'
export const UPDATE_WORD = 'UPDATE_WORD'
export const UPDATE_LOADING = 'UPDATE_LOADING'


export function setIsLogged(value) {
    return { 
        type: UPDATE_ISLOGGED, 
        payload: value 
    }
}

export function setUser(user) {
    return { 
        type: UPDATE_USER, 
        payload: user 
    }
}

export function setLogoutUser() {
    return { 
        type: LOGOUT_USER, 
        payload: {
            isLogged: false,
            user: new UserDisplay()
        }
    }
}


export function setCurrentRoom(value) {
    return {
        type: UPDATE_CURRENTROOM,
        payload: value
    }
}

export function setRooms(rooms) {
    return {
        type: UPDATE_ROOMS,
        payload: rooms
    }
}

export function setCurrentScore(value) {
    return {
        type: UPDATE_CURRENTSCORE,
        payload: value
    }
}

export function setTimer(value) {
    return {
        type: UPDATE_TIMER,
        payload: value
    }
}

export function setWord(value) {
    return {
        type: UPDATE_WORD,
        payload: value
    }
}

export function setLoading(value) {
    return {
        type: UPDATE_LOADING,
        payload: value
    }
}